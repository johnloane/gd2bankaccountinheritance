/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

/**
 *
 * @author administrator
 */
public class CheckingAccount extends BankAccount{
    private int withdrawals;

    /**
     * Constructs a checking account
     * @param accountBalance
     */
    public CheckingAccount(double accountBalance){
        super(accountBalance);
    }
    
    /**
     * Returns the number of withdrawals
     * @return number of withdrawals
     */
    public int getWithDrawals()
    {
        return withdrawals;
    }
    
    //These methods are overridden
    /**
     * Withdraw an amount, 3 free, after that charge €1 per transaction
     * @param amount
     * @return boolean
     */
    @Override
    public boolean withdraw(double amount){
        final int FREE_WITHDRAWALS = 3;
        final int WITHDRAWAL_FEE = 1;
        boolean success;
        //withdrawals++;
        if(++withdrawals > FREE_WITHDRAWALS)
        {
            amount += WITHDRAWAL_FEE;
        }
        success = super.withdraw(amount);
        if(!success)
        {
            withdrawals--;
        }
        return success;    
    }
    /**
     * Sets withdrawals to 0
     */
    @Override
    public void monthEnd(){
        withdrawals = 0;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

import java.util.Scanner;

/**
 *
 * @author administrator
 */
public class Main {
    
    public static void main(String args[]){
        BankAccount[] accounts = new BankAccount[10];
        //creates 5 savingsAccounts and adds to bankaccount array
        createSavingsAccounts(accounts);
        //creates 5 checkingAccounts and adds to bankaccount array
        createCheckingAccounts(accounts);
        getUserInput(accounts);
    }
    
    public static void createSavingsAccounts(BankAccount[] accounts)
    {
        for(int i=0; i < 5; i++)
        {
            SavingsAccount savingAccount = new SavingsAccount(1000, 1);
            accounts[i] = savingAccount;  
        }
    }
    
    public static void createCheckingAccounts(BankAccount[] accounts)
    {
        for(int i=5; i < 10; i++)
        {
            CheckingAccount checkingAccount = new CheckingAccount(1000);
            accounts[i] = checkingAccount;  
        }
    }
    
    public static void getUserInput(BankAccount[] accounts)
    {
        Scanner sc = new Scanner(System.in);
        boolean done = false;
        while(!done)
        {
            System.out.print("D)eposit W)ithdraw M)onth end Q)uit: ");
            String input = sc.next();
            if(input.equals("W") || input.equals("D"))
            {
                System.out.print("Enter account number and amount: ");
                int num = sc.nextInt();
                double amount = sc.nextDouble();
                
                if(input.equals("D"))
                {
                    accounts[num].deposit(amount);
                }
                else
                {
                    accounts[num].withdraw(amount);
                }
                System.out.println("Balance: " + accounts[num].getBalance());
            }
            else if(input.equals("M"))
            {
                for(int n=0; n < accounts.length; n++)
                {
                    accounts[n].monthEnd();
                    System.out.println(n + " " + accounts[n].getBalance());
                }
            }
            else if(input.equals("Q"))
            {
                done = true;
            }
            
        }
    }
            
    
}

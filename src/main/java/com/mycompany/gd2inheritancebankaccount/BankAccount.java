/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

/**
 *
 * @author administrator
 */
public class BankAccount {
    private double balance;
    
    /**
     * Constructor that creates a bank account with a balance
     * @param accountBalance
     */
    public BankAccount(double accountBalance){
        if(accountBalance >= 0.0)
        {
            this.balance = accountBalance;
        }
        else
        {
            this.balance = 0.0;
        }
        
    }
    
    /**
     * Makes a deposit into the account
     * @param amount to deposit
     */
    public void deposit(double amount){
        int decimalPlaces = getDecimalPlaces(amount);
        if(amount > 0 && decimalPlaces <= 2)
        {
            balance += amount;
        }
    }
    
    /**
     * Finds the number of decimal places in a double
     * @param number
     * @return decimalPlaces
     */
    public int getDecimalPlaces(double number)
    {
        String text = Double.toString(Math.abs(number));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        return decimalPlaces;
    }
    
    /**
     * Makes a withdrawal from the account
     * @param amount to withdraw
     * @return proceed
     */
    public boolean withdraw(double amount){
        boolean proceed = false;
        if(amount < this.getBalance())
        {
            proceed = true;
        }
        if(amount > 0 && proceed)
        {
            balance -= amount;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Carries out end of month processing
     * depends on the type of account
     */
    public void monthEnd(){
        
    }
    
    /**
     * Gets the current balance of the account
     * @return current balance
     */
    public double getBalance(){
        return this.balance;
    }
            
            
    
}

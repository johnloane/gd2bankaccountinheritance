/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

/**
 *
 * @author administrator
 */
public class SavingsAccount extends BankAccount{
    private double interestRate;
    private double minBalance;
    
    /**
     * Constructs a savings account with a balance
     * @param accountBalance
     * @param min
     * @param rate
     */
    public SavingsAccount(double accountBalance, double rate){
        super(accountBalance);
        this.minBalance = accountBalance;
        this.interestRate = rate;
    }
    
    /**
     * Sets the interest rate
     * @param rate
     */
    public void setInterestRate(double rate){
        if(rate > 0)
        {
            this.interestRate = rate;
        }
        else
        {
            rate = 0.0;
        }
    }
    
    /**
     * Sets the min balance
     * @param balance
     */
    public void setMinBalance(double balance){
        if(balance > 0)
        {
            this.minBalance = balance;
        }
        else
        {
            balance = 0.0;
        }
    }
    
    /**
     * Gets the interest rate
     * @return rate 
     */
    public double getInterestRate(){
        return interestRate;
    }
    
    /**
     * Returns the min balance in the account over the month
     * @return minBalance
     */
    public double getMinBalance(){
        return minBalance;
    }
            
    
    //override the following methods
    
    /**
     * Withdraws money from the account.
     * @param amount
     * @return boolean
     */
    @Override
    public boolean withdraw(double amount){
        boolean success;
        success = super.withdraw(amount);
        double balance = getBalance();
        if(balance < minBalance)
        {
            minBalance = balance;
        }
        return success;
    }
    @Override
    /**
     * monthEnd works out and adds on the interest
     * interest is calculated on the min balance
     * in the account during the month
     */
    public void monthEnd(){
        double interest = minBalance * (interestRate/100);
        deposit(interest);
        minBalance = getBalance();
    }
}

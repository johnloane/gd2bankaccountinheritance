/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class SavingsAccountTest {
    
    public SavingsAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setInterestRate method, of class SavingsAccount.
     */
    @Test
    public void testSetInterestRate() {
        System.out.println("setInterestRate");
        double rate = 10.0;
        SavingsAccount instance = new SavingsAccount(1000.0, rate);
        instance.setInterestRate(rate);
        double result = instance.getInterestRate();
        assertEquals(rate, result, 0.0);
    }

    /**
     * Test of withdraw method, of class SavingsAccount.
     */
    @Test
    public void testWithdraw() {
        System.out.println("withdraw");
        double amount = 100.0;
        SavingsAccount instance = new SavingsAccount(500, 1);
        instance.withdraw(amount);
        amount = 200.0;
        instance.deposit(amount);
        double actualBalance = instance.getBalance();
        double expectedBalance = 600.0;
        assertEquals(expectedBalance, actualBalance, 0.0);
        double expectedMinBalance = 400.0;
        double actualMinBalance = instance.getMinBalance();
        assertEquals(expectedMinBalance, actualMinBalance, 0.0);
        
    }

    /**
     * Test of monthEnd method, of class SavingsAccount.
     */
    @Test
    public void testMonthEnd() {
        System.out.println("monthEnd");
        SavingsAccount instance = new SavingsAccount(1000, 1);
        instance.withdraw(500);
        instance.deposit(250);
        instance.withdraw(450);
        instance.monthEnd();
        double expectedBalance = 303;
        double actualBalance = instance.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);
    }
    
}

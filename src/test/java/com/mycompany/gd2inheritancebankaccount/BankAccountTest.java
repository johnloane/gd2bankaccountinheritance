/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author administrator
 */
public class BankAccountTest {
    
    public BankAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deposit method, of class BankAccount.
     */
    @Test
    public void testDeposit() {
        System.out.println("deposit");
        double depositAmount = 100.0;
        BankAccount instance = new BankAccount(0.0);
        instance.deposit(depositAmount);
        double expectedResult = 100.0;
        double result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(0.0);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(-100.0);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        instance.deposit(3.33333333);
        result = instance.getBalance();
        assertEquals(expectedResult, result, 0.0);
        
        instance.deposit(97.0);
        result = instance.getBalance();
        expectedResult += 97.0;
        assertEquals(expectedResult, result, 0.0);
    }

    /**
     * Test of withdraw method, of class BankAccount.
     */
    @Test
    public void testWithdraw() 
    {
        System.out.println("withdraw");
        double withdrawAmount = 0.0;
        BankAccount instance = new BankAccount(0.0);
        boolean expResult = false;
        boolean result = instance.withdraw(withdrawAmount);
        assertEquals(expResult, result);
        instance.deposit(100.0);
        result = instance.withdraw(50.0);
        boolean expectedResult = true;
        assertEquals(expectedResult, result);
        testWithdraw(instance, 51.0, 50);
        testWithdraw(instance, -100.0, 50);
        testWithdraw(instance, 0.00, 50);

    }
    
    public void testWithdraw(BankAccount accountToWithDrawFrom, 
            Double amount, double expectedBalance)
    {
        boolean result;
        result = accountToWithDrawFrom.withdraw(51.0);
        boolean expectedResult = false;
        assertEquals(expectedResult, result);
        double actualBalance = accountToWithDrawFrom.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);
    }

    /**
     * Test of monthEnd method, of class BankAccount.
     */
    @Ignore
    public void testMonthEnd() {
        
    }

    /**
     * Test of getBalance method, of class BankAccount.
     */
    @Test
    public void testGetBalance() {
        System.out.println("getBalance");
        BankAccount instance = new BankAccount(100);
        double expResult = 100.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2inheritancebankaccount;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class CheckingAccountTest {
    
    public CheckingAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of withdraw method, of class CheckingAccount.
     */
    @Test
    public void testWithdraw() {
        System.out.println("withdraw");
        double amount = 100.0;
        CheckingAccount instance = new CheckingAccount(1000.0);
        for(int i =0; i < 3; i++)
        {
            instance.withdraw(amount);
        }
        double expectedBalance = 700;
        double actualBalance = instance.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);
        boolean actualResult = instance.withdraw(700);
        boolean expectedResult = false;
        assertEquals(actualResult, expectedResult);
        int expectedWithDrawals = 3;
        int actualWithDrawals = instance.getWithDrawals();
        assertEquals(expectedWithDrawals, actualWithDrawals);
        instance.withdraw(amount);
        expectedBalance = 599;
        actualBalance = instance.getBalance();
        assertEquals(expectedBalance, actualBalance, 0.0);   
    }

    /**
     * Test of monthEnd method, of class CheckingAccount.
     */
    @Test
    public void testMonthEnd() {
        System.out.println("monthEnd");
        CheckingAccount instance = new CheckingAccount(1000);
        double amount = 100.0;
        for(int i =0; i < 3; i++)
        {
            instance.withdraw(amount);
        }
        int expectedWithDrawals = 3;
        int actualWithDrawals = instance.getWithDrawals();
        assertEquals(expectedWithDrawals, actualWithDrawals);
        instance.monthEnd();
        expectedWithDrawals = 0;
        actualWithDrawals = instance.getWithDrawals();
        assertEquals(expectedWithDrawals, actualWithDrawals);
        
    }
    
}
